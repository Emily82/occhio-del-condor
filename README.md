### L'occhio del Condor

Text Adventure Game written for msx and ported for GWBASIC. Game text is in italian. 
Despite MSX, this GWBASIC version can save matches to floppy disk rather than cassette!
